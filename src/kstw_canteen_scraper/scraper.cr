require "uri"
require "http/client"
require "html5"

class KstwCanteenScraper
  CLIENT_COUNT = 8
  @clients : Channel(HTTP::Client)

  def initialize
    @clients = Channel(HTTP::Client).new(CLIENT_COUNT)
    CLIENT_COUNT.times do
      @clients.send(HTTP::Client.new("www.kstw.de", tls: true))
    end
  end

  private def borrow_client(&block : HTTP::Client -> T) : T forall T
    client = @clients.receive
    ret = yield client
    @clients.send(client)
    ret
  end

  private def begin_print_status(task_name : String, tasks_total : Int32, tasks_done : Int32)
    LibC.printf "#{task_name} [#{tasks_done}/#{tasks_total}]          \n"
  end

  private def print_status(task_name : String, tasks_total : Int32, tasks_done : Int32)
    LibC.printf "\r\033[1A#{task_name} [#{tasks_done}/#{tasks_total}]          \n"
  end

  def fetch_data : Data
    begin_print_status("Fetching index", 1, 0)

    uri = URI.parse("https://www.kstw.de/speiseplan")
    location = Time::Location.load("Europe/Berlin")
    res = borrow_client(&.get("/speiseplan"))
    raise RuntimeError.new("Error code #{res.status_code} while fetching /speiseplan") unless res.success?
    page1_html = HTML5.parse(res.body)

    # css =

    menu_lines = page1_html.css(".lightbox-container[data-lightbox-group=\"legend\"] .tx-epwerkmenu-menu-meal-left").map do |menu_line|
      MenuLine.new(
        id: menu_line.css(".category-title").first.inner_text.strip,
        description: menu_line.css(".category-text").first.inner_text.strip,
        icon_url: uri.resolve(menu_line.css("img").first["src"].val).to_s
      )
    end

    locations = page1_html.css(".tx-epwerkmenu-menu-location-wrapper").map do |location|
      title_element = location.css(".tx-epwerkmenu-menu-location-title").first
      location_info_link = title_element.css("a").first["href"]
      Location.new(
        id: location["data-location"].val.strip,
        name: title_element.inner_text.strip,
      )
    end

    additives = nil
    allergens = nil
    additives_allergens_parsed = false

    dates = page1_html.css(".dates input[data-id]").map(&.["data-id"].val)

    print_status("Fetching index", 1, 1)
    begin_print_status("Fetching canteen data", dates.size, 0)
    progress = 0

    wait_channel = Channel(Nil).new

    dishes = [] of Dish
    dates.each_with_index do |date, i|
      spawn(same_thread: true) do
        begin
          time = Time.parse(date, "%F", location)
          res = borrow_client(&.get("/speiseplan?t=#{date}"))
          raise RuntimeError.new("Error code #{res.status_code} while fetching /speiseplan") unless res.success?
          page_date_html = HTML5.parse(res.body)

          unless additives_allergens_parsed
            additives_allergens_list = page_date_html.css(".additivesAllergensOthers_list")
            unless additives_allergens_list.empty?
              additives_allergens_list.first.css(".col-12").each do |category_list|
                title = category_list.css(".title").first.inner_text.strip.rchop(':')
                ret = [] of Additive
                current_element = category_list.first_child
                while current_element
                  inner_text = current_element.inner_text
                  if inner_text.includes?('=')
                    split = inner_text.split('=')
                    ret << Additive.new(split[0].strip, split[1].strip)
                  end
                  current_element = current_element.next_sibling
                end

                case title
                when "Allergene"
                  allergens = ret
                when "Zusatzstoffe"
                  additives = ret
                end
              end
              additives_allergens_parsed = true
            end
          end

          page_date_html.css(".tx-epwerkmenu-menu-location-wrapper").each do |location_elem|
            location_id = location_elem["data-location"].val

            location_parts = location_elem.css(".tx-epwerkmenu-menu-locationpart-wrapper").map do |part|
              location_part = part["data-locationpart"].val == "default" ? nil : part.css(".tx-epwerkmenu-menu-locationpart-title").first.inner_text
              {location_part, part}
            end

            location_parts.each do |part_name, element|
              menu_times = element.css(".tx-epwerkmenu-menu-times-wrapper").map do |time_elem|
                times_of_day = time_elem["data-locationpart"].val
                if times_of_day == "default"
                  {time, time.at_end_of_day, element}
                else
                  times_of_day_parsed = times_of_day.split('-').map { |tm| Time.parse(tm.strip, "%R", location).time_of_day }
                  {time + times_of_day_parsed[0], time + times_of_day_parsed[1], time_elem}
                end
              end

              menu_times.each do |time_begin, time_end, element|
                element.css(".menue-tile").each do |menu_tile|
                  prices = menu_tile.css(".tx-epwerkmenu-menu-meal-prices").first.inner_text.gsub(',', '.').split('/').map &.to_f?

                  dishes << Dish.new(
                    name: menu_tile.css(".tx-epwerkmenu-menu-meal-title").first.inner_text.strip,
                    student_price: prices[0],
                    staff_price: prices[1],
                    guest_price: prices[2],
                    description: menu_tile.css(".tx-epwerkmenu-menu-meal-description").first.inner_text.strip,
                    menu_line_id: menu_tile["data-category"].val.downcase.strip,
                    allergen_ids: menu_tile["data-allergene"].val.split(' ', remove_empty: true).map(&.strip),
                    additive_ids: menu_tile["data-zusatzstoffe"].val.split(' ', remove_empty: true).map(&.strip),
                    location_id: location_id,
                    location_part: part_name,
                    image_url: menu_tile.css(".plate > img").first?.try(&.["src"].val),
                    time_begin: time_begin,
                    time_end: time_end,
                  )
                end
              end
            end
          end

          progress += 1
          print_status("Fetching canteen data", dates.size, progress)
          wait_channel.send(nil)
        rescue ex
          abort("Fatal error while fetching data: #{ex.inspect_with_backtrace}")
        end
      end
    end

    dates.size.times { wait_channel.receive }

    raise RuntimeError.new("Missing additives or allergens") unless additives && allergens

    Data.new(
      allergens: allergens.not_nil!,
      additives: additives.not_nil!,
      menu_lines: menu_lines,
      dishes: dishes,
      locations: locations,
    )
  end
end
