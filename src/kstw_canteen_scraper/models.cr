require "json"

record KstwCanteenScraper::Data,
  allergens : Array(Additive),
  additives : Array(Additive),
  menu_lines : Array(MenuLine),
  dishes : Array(Dish),
  locations : Array(Location) do
  include JSON::Serializable
end

record KstwCanteenScraper::Dish,
  name : String,
  description : String,
  image_url : String?,
  menu_line_id : String,
  student_price : Float64?,
  staff_price : Float64?,
  guest_price : Float64?,
  allergen_ids : Array(String),
  additive_ids : Array(String),
  time_begin : Time,
  time_end : Time,
  location_id : String,
  location_part : String? do
  include JSON::Serializable

  @[JSON::Field(emit_null: true)]
  @student_price : Float64?

  @[JSON::Field(emit_null: true)]
  @staff_price : Float64?

  @[JSON::Field(emit_null: true)]
  @guest_price : Float64?

  @[JSON::Field(emit_null: true)]
  @image_url : String?

  @[JSON::Field(emit_null: true)]
  @location_part : String?
end

record KstwCanteenScraper::Additive,
  id : String,
  name : String do
  include JSON::Serializable
end

record KstwCanteenScraper::MenuLine,
  id : String,
  description : String,
  icon_url : String do
  include JSON::Serializable
end

record KstwCanteenScraper::Location,
  id : String,
  name : String do
  include JSON::Serializable
end
