require "./kstw_canteen_scraper/scraper"
require "./kstw_canteen_scraper/models"
require "option_parser"

def self.main
  destination = "./output.json"

  OptionParser.parse do |parser|
    parser.banner = "Usage: kstw_canteen_scraper [arguments]"
    parser.on("-o PATH", "--output PATH", "Sets the output file") { |string| destination = string }
    parser.on("-h", "--help", "Show this help") do
      puts parser
      exit
    end
    parser.invalid_option do |flag|
      STDERR.puts "ERROR: #{flag} is not a valid option."
      STDERR.puts parser
      exit(1)
    end
  end

  Dir.mkdir_p(Path[destination].parent)

  File.open(destination, "w") do |io|
    KstwCanteenScraper.new.fetch_data.to_json(io)
  end
end

main()
