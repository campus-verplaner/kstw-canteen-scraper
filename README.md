# kstw_canteen_scraper

A scraper for the canteens operated by the "Kölner Studierendenwerk".

## Installation

Install the shards using `shards` and compile the program with `crystal build --release src/kstw_canteen_scraper.cr`.

## Usage

The program currently accepts no arguments. Just execute the binary and after a few minutes, a file called `output.json` will be created in your current directory.

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/BlobCodes/kstw-canteen-scraper/-/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [David Keller](https://gitlab.com/BlobCodes) - creator and maintainer
